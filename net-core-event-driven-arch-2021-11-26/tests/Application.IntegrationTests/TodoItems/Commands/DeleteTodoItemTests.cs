﻿using FluentAssertions;
using net_core_event_driven_arch_2021_11_26.Application.Common.Exceptions;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.CreateTodoItem;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.DeleteTodoItem;
using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Commands.CreateTodoList;
using net_core_event_driven_arch_2021_11_26.Domain.Entities;
using NUnit.Framework;
using static Testing;

namespace net_core_event_driven_arch_2021_11_26.Application.IntegrationTests.TodoItems.Commands
{
    public class DeleteTodoItemTests : TestBase
    {
        [Test]
        public async Task ShouldRequireValidTodoItemId()
        {
            var command = new DeleteTodoItemCommand { Id = 99 };

            await FluentActions.Invoking(() =>
                SendAsync(command)).Should().ThrowAsync<NotFoundException>();
        }

        [Test]
        public async Task ShouldDeleteTodoItem()
        {
            var listId = await SendAsync(new CreateTodoListCommand
            {
                Title = "New List"
            });

            var itemId = await SendAsync(new CreateTodoItemCommand
            {
                ListId = listId,
                Title = "New Item"
            });

            await SendAsync(new DeleteTodoItemCommand
            {
                Id = itemId
            });

            var item = await FindAsync<TodoItem>(itemId);

            item.Should().BeNull();
        }
    }
}