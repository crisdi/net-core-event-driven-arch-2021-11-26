﻿using FluentAssertions;
using net_core_event_driven_arch_2021_11_26.Application.Common.Exceptions;
using net_core_event_driven_arch_2021_11_26.Application.Common.Security;
using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Commands.CreateTodoList;
using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Commands.PurgeTodoLists;
using net_core_event_driven_arch_2021_11_26.Domain.Entities;
using NUnit.Framework;
using static Testing;

namespace net_core_event_driven_arch_2021_11_26.Application.IntegrationTests.TodoLists.Commands
{
    public class PurgeTodoListsTests : TestBase
    {
        [Test]
        public async Task ShouldDenyAnonymousUser()
        {
            var command = new PurgeTodoListsCommand();

            command.GetType().Should().BeDecoratedWith<AuthorizeAttribute>();

            await FluentActions.Invoking(() =>
                SendAsync(command)).Should().ThrowAsync<UnauthorizedAccessException>();
        }

        [Test]
        public async Task ShouldDenyNonAdministrator()
        {
            await RunAsDefaultUserAsync();

            var command = new PurgeTodoListsCommand();

            await FluentActions.Invoking(() =>
                 SendAsync(command)).Should().ThrowAsync<ForbiddenAccessException>();
        }

        [Test]
        public async Task ShouldAllowAdministrator()
        {
            await RunAsAdministratorAsync();

            var command = new PurgeTodoListsCommand();

            await FluentActions.Invoking(() => SendAsync(command))
                 .Should().NotThrowAsync<ForbiddenAccessException>();
        }

        [Test]
        public async Task ShouldDeleteAllLists()
        {
            await RunAsAdministratorAsync();

            await SendAsync(new CreateTodoListCommand
            {
                Title = "New List #1"
            });

            await SendAsync(new CreateTodoListCommand
            {
                Title = "New List #2"
            });

            await SendAsync(new CreateTodoListCommand
            {
                Title = "New List #3"
            });

            await SendAsync(new PurgeTodoListsCommand());

            var count = await CountAsync<TodoList>();

            count.Should().Be(0);
        }
    }
}