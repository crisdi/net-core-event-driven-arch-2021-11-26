﻿using NUnit.Framework;
using static Testing;

namespace net_core_event_driven_arch_2021_11_26.Application.IntegrationTests
{
    public class TestBase
    {
        [SetUp]
        public async Task TestSetUp()
        {
            await ResetState();
        }
    }
}