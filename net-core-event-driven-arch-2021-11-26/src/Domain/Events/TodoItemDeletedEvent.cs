﻿namespace net_core_event_driven_arch_2021_11_26.Domain.Events
{
    public class TodoItemDeletedEvent : DomainEvent
    {
        public TodoItemDeletedEvent(TodoItem item)
        {
            Item = item;
        }

        public TodoItem Item { get; }
    }
}