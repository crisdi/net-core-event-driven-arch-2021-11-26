﻿namespace net_core_event_driven_arch_2021_11_26.Domain.Enums
{
    public enum PriorityLevel
    {
        None = 0,
        Low = 1,
        Medium = 2,
        High = 3
    }
}