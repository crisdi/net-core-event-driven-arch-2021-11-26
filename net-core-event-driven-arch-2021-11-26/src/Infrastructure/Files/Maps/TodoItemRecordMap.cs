﻿using CsvHelper.Configuration;
using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.ExportTodos;
using System.Globalization;

namespace net_core_event_driven_arch_2021_11_26.Infrastructure.Files.Maps
{
    public class TodoItemRecordMap : ClassMap<TodoItemRecord>
    {
        public TodoItemRecordMap()
        {
            AutoMap(CultureInfo.InvariantCulture);

            Map(m => m.Done).ConvertUsing(c => c.Done ? "Yes" : "No");
        }
    }
}