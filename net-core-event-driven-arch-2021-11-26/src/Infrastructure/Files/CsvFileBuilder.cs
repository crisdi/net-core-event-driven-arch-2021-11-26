﻿using CsvHelper;
using net_core_event_driven_arch_2021_11_26.Application.Common.Interfaces;
using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.ExportTodos;
using net_core_event_driven_arch_2021_11_26.Infrastructure.Files.Maps;
using System.Globalization;

namespace net_core_event_driven_arch_2021_11_26.Infrastructure.Files
{
    public class CsvFileBuilder : ICsvFileBuilder
    {
        public byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records)
        {
            using var memoryStream = new MemoryStream();
            using (var streamWriter = new StreamWriter(memoryStream))
            {
                using var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture);

                csvWriter.Configuration.RegisterClassMap<TodoItemRecordMap>();
                csvWriter.WriteRecords(records);
            }

            return memoryStream.ToArray();
        }
    }
}