﻿using Microsoft.AspNetCore.Identity;
using net_core_event_driven_arch_2021_11_26.Application.Common.Models;

namespace net_core_event_driven_arch_2021_11_26.Infrastructure.Identity
{
    public static class IdentityResultExtensions
    {
        public static Result ToApplicationResult(this IdentityResult result)
        {
            return result.Succeeded
                ? Result.Success()
                : Result.Failure(result.Errors.Select(e => e.Description));
        }
    }
}