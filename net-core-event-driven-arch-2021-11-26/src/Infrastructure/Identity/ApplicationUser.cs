﻿using Microsoft.AspNetCore.Identity;

namespace net_core_event_driven_arch_2021_11_26.Infrastructure.Identity
{
    public class ApplicationUser : IdentityUser
    {
    }
}