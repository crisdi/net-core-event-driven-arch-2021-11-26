﻿using net_core_event_driven_arch_2021_11_26.Application.Common.Interfaces;

namespace net_core_event_driven_arch_2021_11_26.Infrastructure.Services
{
    public class DateTimeService : IDateTime
    {
        public DateTime Now => DateTime.Now;
    }
}