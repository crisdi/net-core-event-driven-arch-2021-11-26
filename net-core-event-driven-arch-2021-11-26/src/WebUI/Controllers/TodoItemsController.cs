﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using net_core_event_driven_arch_2021_11_26.Application.Common.Models;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.CreateTodoItem;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.DeleteTodoItem;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.UpdateTodoItem;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Commands.UpdateTodoItemDetail;
using net_core_event_driven_arch_2021_11_26.Application.TodoItems.Queries.GetTodoItemsWithPagination;

namespace net_core_event_driven_arch_2021_11_26.WebUI.Controllers
{
    [Authorize]
    public class TodoItemsController : ApiControllerBase
    {
        [HttpGet]
        public async Task<ActionResult<PaginatedList<TodoItemBriefDto>>> GetTodoItemsWithPagination([FromQuery] GetTodoItemsWithPaginationQuery query)
        {
            return await Mediator.Send(query);
        }

        [HttpPost]
        public async Task<ActionResult<int>> Create(CreateTodoItemCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Update(int id, UpdateTodoItemCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpPut("[action]")]
        public async Task<ActionResult> UpdateItemDetails(int id, UpdateTodoItemDetailCommand command)
        {
            if (id != command.Id)
            {
                return BadRequest();
            }

            await Mediator.Send(command);

            return NoContent();
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            await Mediator.Send(new DeleteTodoItemCommand { Id = id });

            return NoContent();
        }
    }
}