﻿using Microsoft.AspNetCore.Mvc;
using net_core_event_driven_arch_2021_11_26.Application.WeatherForecasts.Queries.GetWeatherForecasts;

namespace net_core_event_driven_arch_2021_11_26.WebUI.Controllers
{
    public class WeatherForecastController : ApiControllerBase
    {
        [HttpGet]
        public async Task<IEnumerable<WeatherForecast>> Get()
        {
            return await Mediator.Send(new GetWeatherForecastsQuery());
        }
    }
}