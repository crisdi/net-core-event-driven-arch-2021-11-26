﻿using MediatR;
using Microsoft.Extensions.Logging;
using net_core_event_driven_arch_2021_11_26.Application.Common.Models;
using net_core_event_driven_arch_2021_11_26.Domain.Events;

namespace net_core_event_driven_arch_2021_11_26.Application.TodoItems.EventHandlers
{
    public class TodoItemCompletedEventHandler : INotificationHandler<DomainEventNotification<TodoItemCompletedEvent>>
    {
        private readonly ILogger<TodoItemCompletedEventHandler> _logger;

        public TodoItemCompletedEventHandler(ILogger<TodoItemCompletedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(DomainEventNotification<TodoItemCompletedEvent> notification, CancellationToken cancellationToken)
        {
            var domainEvent = notification.DomainEvent;

            _logger.LogInformation("net_core_event_driven_arch_2021_11_26 Domain Event: {DomainEvent}", domainEvent.GetType().Name);

            return Task.CompletedTask;
        }
    }
}