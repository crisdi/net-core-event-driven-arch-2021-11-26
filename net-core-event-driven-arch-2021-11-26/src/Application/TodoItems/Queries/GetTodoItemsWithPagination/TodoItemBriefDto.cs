﻿using net_core_event_driven_arch_2021_11_26.Application.Common.Mappings;
using net_core_event_driven_arch_2021_11_26.Domain.Entities;

namespace net_core_event_driven_arch_2021_11_26.Application.TodoItems.Queries.GetTodoItemsWithPagination
{
    public class TodoItemBriefDto : IMapFrom<TodoItem>
    {
        public int Id { get; set; }

        public int ListId { get; set; }

        public string? Title { get; set; }

        public bool Done { get; set; }
    }
}