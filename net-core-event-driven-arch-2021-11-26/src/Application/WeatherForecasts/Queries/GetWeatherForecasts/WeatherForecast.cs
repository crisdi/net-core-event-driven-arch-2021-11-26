﻿namespace net_core_event_driven_arch_2021_11_26.Application.WeatherForecasts.Queries.GetWeatherForecasts
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string? Summary { get; set; }
    }
}