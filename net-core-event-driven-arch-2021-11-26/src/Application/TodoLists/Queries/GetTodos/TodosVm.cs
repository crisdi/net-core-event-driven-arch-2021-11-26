﻿namespace net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.GetTodos
{
    public class TodosVm
    {
        public IList<PriorityLevelDto> PriorityLevels { get; set; } = new List<PriorityLevelDto>();

        public IList<TodoListDto> Lists { get; set; } = new List<TodoListDto>();
    }
}