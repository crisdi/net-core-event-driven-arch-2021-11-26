﻿using net_core_event_driven_arch_2021_11_26.Application.Common.Mappings;
using net_core_event_driven_arch_2021_11_26.Domain.Entities;

namespace net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.GetTodos
{
    public class TodoListDto : IMapFrom<TodoList>
    {
        public TodoListDto()
        {
            Items = new List<TodoItemDto>();
        }

        public int Id { get; set; }

        public string? Title { get; set; }

        public string? Colour { get; set; }

        public IList<TodoItemDto> Items { get; set; }
    }
}