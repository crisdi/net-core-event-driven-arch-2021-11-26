﻿namespace net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.GetTodos
{
    public class PriorityLevelDto
    {
        public int Value { get; set; }

        public string? Name { get; set; }
    }
}