﻿using AutoMapper;

namespace net_core_event_driven_arch_2021_11_26.Application.Common.Mappings
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}