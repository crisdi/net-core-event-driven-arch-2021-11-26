﻿using MediatR;
using net_core_event_driven_arch_2021_11_26.Domain.Common;

namespace net_core_event_driven_arch_2021_11_26.Application.Common.Models
{
    public class DomainEventNotification<TDomainEvent> : INotification where TDomainEvent : DomainEvent
    {
        public DomainEventNotification(TDomainEvent domainEvent)
        {
            DomainEvent = domainEvent;
        }

        public TDomainEvent DomainEvent { get; }
    }
}