﻿using net_core_event_driven_arch_2021_11_26.Application.TodoLists.Queries.ExportTodos;

namespace net_core_event_driven_arch_2021_11_26.Application.Common.Interfaces
{
    public interface ICsvFileBuilder
    {
        byte[] BuildTodoItemsFile(IEnumerable<TodoItemRecord> records);
    }
}