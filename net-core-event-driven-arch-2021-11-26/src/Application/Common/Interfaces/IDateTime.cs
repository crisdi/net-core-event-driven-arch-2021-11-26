﻿namespace net_core_event_driven_arch_2021_11_26.Application.Common.Interfaces
{
    public interface IDateTime
    {
        DateTime Now { get; }
    }
}